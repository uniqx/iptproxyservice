package info.pluggabletransports.service

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class PTConfigureActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ptconfigure)
    }
}